package com.zcp.demo.layout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zcp.demo.R

class RelativeLayoutDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relative_layout_demo)
    }
}