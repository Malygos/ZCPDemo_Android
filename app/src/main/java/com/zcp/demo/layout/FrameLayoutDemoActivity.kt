package com.zcp.demo.layout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zcp.demo.R

class FrameLayoutDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame_layout_demo)
    }
}