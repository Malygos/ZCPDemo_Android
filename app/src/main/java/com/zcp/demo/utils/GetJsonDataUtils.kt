package com.zcp.demo.utils

import android.content.Context
import android.content.res.AssetManager
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.StringBuilder

object GetJsonDataUtils {
    fun getJson(context: Context, fileName: String): String {
        val stringBuilder = StringBuilder()

        try {
            val assetManager: AssetManager = context.assets
            val bf = BufferedReader(InputStreamReader(assetManager.open(fileName)))
            var line: String? = bf.readLine()

            while (line != null) {
                stringBuilder.append(line)
                line = bf.readLine()
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return stringBuilder.toString()
    }
}