package com.zcp.demo.main

import android.content.Context
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zcp.demo.utils.GetJsonDataUtils

class DemosViewModel : ViewModel() {
    lateinit var demoList: List<DemoInfo>
    var titleList: List<String> = listOf();get() {
        var list = mutableListOf<String>()
        for (demoInfo in demoList) {
            list.add(demoInfo.title)
        }
        return list.toList()
    }

    fun getDemoList(context: Context) {
        val jsonData = GetJsonDataUtils.getJson(context, "demo_info.json")
        val typeOf = object : TypeToken<List<DemoInfo>>() {}.type
        demoList = Gson().fromJson(jsonData, typeOf)
    }
}