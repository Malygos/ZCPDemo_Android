package com.zcp.demo.main

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DemoInfo(
    var title: String = "",
    var className: String = "",
    var list: List<DemoInfo> = listOf()
): Parcelable