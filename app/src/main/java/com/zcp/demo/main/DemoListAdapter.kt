package com.zcp.demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ListAdapter

class DemoListAdapter(private val onClick: (String, Int) -> Unit) : ListAdapter<String, DemoListAdapter.DemoListViewHolder>(DemoDiffCallback) {

    class DemoListViewHolder(view: View, val onClick: (String, Int) -> Unit) : RecyclerView.ViewHolder(view) {
        private val textView: TextView = view.findViewById(R.id.item_text)
        private var currentItem: String? = null
        private var currentPosition: Int = 0

        init {
            view.setOnClickListener {
                currentItem?.let {
                    onClick(it, currentPosition)
                }
            }
        }

        fun bind(item: String, position: Int) {
            currentItem = item
            currentPosition = position
            textView.text = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_item_demo, parent, false)
        return DemoListViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: DemoListViewHolder, position: Int) {
        val item: String = getItem(position)
        holder.bind(item, position)
    }
}

object DemoDiffCallback : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}