package com.zcp.demo.main

import android.app.Activity
import android.content.*
import android.content.res.AssetManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.zcp.demo.*
import com.zcp.demo.broadcast.BroadcastDemoActivity
import com.zcp.demo.provider.ProviderDemoActivity
import com.zcp.demo.service.ServiceDemoActivity
import com.zcp.demo.utils.GetJsonDataUtils
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {
    private val viewModel = DemosViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
    }

    private fun setupView() {
        if (intent == null) {
            viewModel.getDemoList(this)
        } else {
            val isMainActivity = intent.getBooleanExtra("isMainActivity", true)
            if (isMainActivity) {
                viewModel.getDemoList(this)
            } else {
                val list =  intent.getParcelableArrayListExtra<DemoInfo>("list")
                if (list != null) {
                    viewModel.demoList = list
                }
            }
        }

        val listAdapter = DemoListAdapter { item: String, position: Int -> adapterOnClick(item, position)}
        demo_list.adapter = listAdapter
        listAdapter.submitList(viewModel.titleList)
    }

    private fun adapterOnClick(info: String, position: Int) {
        val demoInfo = viewModel.demoList[position]
        val className = demoInfo.className
        val list = demoInfo.list
        val arrayList: ArrayList<DemoInfo> = arrayListOf()
        arrayList.addAll(list)
        val intent: Intent?

        if (className.isNotEmpty()) {
            val cls = Class.forName(demoInfo.className)
            intent = Intent(this, cls)
        } else {
            intent = Intent(this, MainActivity::class.java)
            intent.putExtra("isMainActivity", false)
            intent.putParcelableArrayListExtra("list", arrayList)
        }

        startActivity(intent)
    }
}