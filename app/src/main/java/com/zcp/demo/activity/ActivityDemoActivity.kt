package com.zcp.demo.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.zcp.demo.R

/**
 * 1.跳转到Activity
 *      - onCreate
 *      - onStart
 *      - onResume
 * 2.退出Activity
 *      - onPause
 *      - onStop
 *      - onDestroy
 * 3.App切换/点击物理切换按钮
 *      - onPause
 *      - onStop
 * 4.切回App
 *      - onStart
 *      - onResume
 * 5.跳转到下一个Activity
 *      - onPause
 *      - onCreate(下一个)
 *      - onStart(下一个)
 *      - onResume(下一个)
 *      - onStop
 * 6.从下一个Activity返回
 *      - onPause(下一个)
 *      - onStart
 *      - onResume
 *      - onStop(下一个)
 *      - onDestroy(下一个)
 */
class ActivityDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity_demo)
        Log.d("ActivityDemo:","The onCreate() event")

        val textView: TextView = findViewById(R.id.demo_textView)
        textView.setOnClickListener {
            val intent = Intent(this, ActivityDemoActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("ActivityDemo: ","The onStart() event")
    }

    override fun onResume() {
        super.onResume()
        Log.d("ActivityDemo: ","The onResume() event")
    }

    override fun onPause() {
        super.onPause()
        Log.d("ActivityDemo: ","The onPause() event")
    }

    override fun onStop() {
        super.onStop()
        Log.d("ActivityDemo: ","The onStop() event")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ActivityDemo: ","The onDestroy() event")
    }
}