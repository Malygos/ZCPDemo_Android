package com.zcp.demo.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast

class ZCPService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "服务已经启动", Toast.LENGTH_LONG).show()
        return START_STICKY
    }
    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "服务已经停止", Toast.LENGTH_LONG).show()
    }
}