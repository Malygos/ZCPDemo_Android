package com.zcp.demo.service

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.zcp.demo.R

class ServiceDemoActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_demo)

        val startServiceTextView: TextView = findViewById(R.id.start_service_tv)
        val stopServiceTextView: TextView = findViewById(R.id.stop_service_tv)

        startServiceTextView.setOnClickListener {
            startService(Intent(baseContext, ZCPService::class.java))
        }
        stopServiceTextView.setOnClickListener {
            stopService(Intent(baseContext, ZCPService::class.java))
        }
    }
}