package com.zcp.demo.broadcast

import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.zcp.demo.R

class BroadcastDemoActivity : AppCompatActivity() {
    lateinit var receiver: ZCPReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast_demo)

        receiver = ZCPReceiver()
        val filter = IntentFilter()
        filter.addAction(receiver.ACTION_DO_WORK)
        registerReceiver(receiver, filter)

        val sendBtn: Button = findViewById(R.id.broadcast_send_btn)
        sendBtn.setOnClickListener {
            val intent = Intent(receiver.ACTION_DO_WORK)
            intent.setPackage(packageName)
            sendBroadcast(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }
}