package com.zcp.demo.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

class ZCPReceiver: BroadcastReceiver() {
    val ACTION_DO_WORK: String = "com.zcp.DO_WORK"

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == ACTION_DO_WORK) {
            Toast.makeText(context, "收到通知：去工作", Toast.LENGTH_LONG).show()
        }
    }
}