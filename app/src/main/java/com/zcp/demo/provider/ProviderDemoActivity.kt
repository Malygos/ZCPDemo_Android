package com.zcp.demo.provider

import android.content.ContentResolver
import android.content.ContentValues
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.Toast
import com.zcp.demo.R

class ProviderDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provider_demo)

        val resolver: ContentResolver = contentResolver
        val insertBtn: Button = findViewById(R.id.provider_insert_btn)
        insertBtn.setOnClickListener {
            val values = ContentValues()
            values.put("name", "测试")
            val uri = Uri.parse("content://com.zcp.demo.providers.zcpprovider/test")
            resolver.insert(uri, values)
            Toast.makeText(applicationContext, "数据插入成功", Toast.LENGTH_SHORT).show()
        }

        val addTelBtn: Button = findViewById(R.id.provider_add_tel_btn)
        addTelBtn.setOnClickListener {
            val resolver = contentResolver
            val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            val cursor = resolver.query(uri, null, null, null, null) ?: return@setOnClickListener

            while (cursor.moveToNext()) {
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                val num = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                println("姓名：$name")
                println("号码：$num")
                println("===================")
            }

            cursor.close()
        }
    }
}