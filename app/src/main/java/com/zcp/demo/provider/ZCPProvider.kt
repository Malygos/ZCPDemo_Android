package com.zcp.demo.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.widget.Switch

class ZCPProvider : ContentProvider() {
    val matcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)
    lateinit var dbOpenHelper: DBOpenHelper

    override fun onCreate(): Boolean {
        matcher.addURI("com.zcp.demo.providers.zcpprovider", "test", 1)
        dbOpenHelper = DBOpenHelper(context, "test.db", null, 1)
        return true
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        when(matcher.match(uri)) {
            1 -> {
                val db: SQLiteDatabase = dbOpenHelper.readableDatabase
                val rowId: Long = db.insert("test", null, values)
                if (rowId > 0) {
                    val nameUri: Uri = ContentUris.withAppendedId(uri, rowId)
                    context?.contentResolver?.notifyChange(nameUri, null)
                    return nameUri
                }
            }
        }
        return null
    }

    override fun query(uri: Uri, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
        return null
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<out String>?): Int {
        return 0
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        return 0
    }
}