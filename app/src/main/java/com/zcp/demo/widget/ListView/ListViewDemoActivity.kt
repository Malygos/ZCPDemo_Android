package com.zcp.demo.widget.ListView

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.zcp.demo.R
import kotlinx.android.synthetic.main.activity_list_view_demo.*

class ListViewDemoActivity : AppCompatActivity() {
    private val data = listOf(
        "Apple", "Banana", "Orange", "Watermelon", "Pear",
        "Grape", "Pineapple", "Strawberry", "Cherry", "Mango",
        "Apple", "Banana", "Orange", "Watermelon", "Pear",
        "Grape", "Pineapple", "Strawberry", "Cherry", "Mango"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view_demo)

        val adapter = ArrayAdapter<String>(this, R.layout.list_item_fruit, data)
        list_view.adapter = adapter
    }
}