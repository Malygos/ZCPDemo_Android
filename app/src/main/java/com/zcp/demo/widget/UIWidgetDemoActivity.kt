package com.zcp.demo.widget

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.zcp.demo.R

class UIWidgetDemoActivity : AppCompatActivity() {
    var imageIsNormal: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ui_widget_demo)

        val textView: TextView = findViewById(R.id.widget_text)
        val button: Button = findViewById(R.id.widget_button)
        val editText: EditText = findViewById(R.id.widget_edit)
        val imageView: ImageView = findViewById(R.id.widget_image)
        val cycleProgressBar: ProgressBar = findViewById(R.id.widget_progress_cycle)
        val horProgressBar: ProgressBar = findViewById(R.id.widget_progress_hor)
        val showDialogButton: Button = findViewById(R.id.widget_show_dialog_btn)

        button.setOnClickListener {
            val text = editText.text.toString()
            textView.text = text
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        }

        imageView.setOnClickListener {
            imageIsNormal = !imageIsNormal
            if (imageIsNormal) {
                imageView.setImageResource(R.mipmap.img_mbg_normal)
            } else {
                imageView.setImageResource(R.mipmap.img_mbg_beaten)
            }
        }

        cycleProgressBar.setOnClickListener {
            cycleProgressBar.visibility = View.GONE
        }

        horProgressBar.setOnClickListener {
            if (horProgressBar.progress == 100) {
                horProgressBar.visibility = View.GONE
            } else {
                horProgressBar.progress += 10
            }
        }

        showDialogButton.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setTitle("This is Dialog")
                setMessage("Something important")
                setCancelable(false)
                setPositiveButton("OK") { dialog, which ->
                }
                setNegativeButton("Cancel") { dialog, which ->
                }
                show()
            }
        }
    }
}