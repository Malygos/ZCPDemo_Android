package com.zcp.demo.widget

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zcp.demo.R

class UICustomWidgetDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ui_custom_widget_demo)
        supportActionBar?.hide()
    }
}