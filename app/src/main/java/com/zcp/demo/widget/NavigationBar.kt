package com.zcp.demo.widget

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.Toast
import com.zcp.demo.R
import kotlinx.android.synthetic.main.view_nav_bar.view.*

class NavigationBar(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    init {
        LayoutInflater.from(context).inflate(R.layout.view_nav_bar, this)

        nav_bar_back_btn.setOnClickListener {
            val activity = context as Activity
            activity.finish()
        }

        nav_bar_edit_btn.setOnClickListener {
            Toast.makeText(context, "You clicked Edit button.", Toast.LENGTH_SHORT).show()
        }
    }
}